# SQL table relations

## TO START 
1.  Input your informations in the `connection.php` file.
2.  Connect to your local host and import the included database (`ToDo.sql`). 
3.  Open a localhost server.

## FUNCTIONALITY
1.  `index.php`, the main page, allows you to see which task is assigned to who. 
    the button `modify` links to `modify.php`. 
    You may also `delete` an assginement. 
    There is also a button that links to `assign_task_final.php` to `add` assignements.
    There are buttons that link to the individual tables for the people and the tasks.
2.  `modify.php` lets you `modify` an assigned task; both the task and the person assigned can be modified.
3.  `assign_task_final.php` lets you `add` new assignments. You may add multiple people at once for the same task. 
2.  `task.php` lets you see all the currently existing tasks. You can also `delete` a task or `add` a new task.
3.  `people.php` lets you see all the currently exisiting people in your database. It also lets you `delete` or `add` individuals.

## IMPORTANT NOTE 
1.  In order to avoid unnecessary duplicates, if a task has already being assigned to someone, the task cannot be assigned to the same person again. 
2.  In order to avoid unnecessary duplicates, a task cannot be added if it shares the same name as an already existing task.
3.  In order to avoid unnecessary duplicates, a person cannot be added if they have the exact same name as an already existing person.
4.  Be careful, deleting a task, or deleting a person will remove them from the assigned table as well since they don't exist anymore.
5.  Please mind capitalization when adding tasks or people. It will be taken into account. If you wish to add a task or a person with the same name, changing the capitalization to differ from the already existing task or person will add them. 
