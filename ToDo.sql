-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 12, 2021 at 11:13 PM
-- Server version: 8.0.27-0ubuntu0.20.04.1
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ToDo`
--

-- --------------------------------------------------------

--
-- Table structure for table `assigned_tasks3`
--

CREATE TABLE `assigned_tasks3` (
  `id` int NOT NULL,
  `task` int NOT NULL,
  `assigned_person` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `assigned_tasks3`
--

INSERT INTO `assigned_tasks3` (`id`, `task`, `assigned_person`) VALUES
(17, 1, 2),
(18, 1, 21),
(23, 2, 3),
(24, 2, 5),
(25, 2, 7),
(26, 2, 11),
(27, 2, 14),
(28, 2, 21),
(47, 3, 3),
(46, 3, 6),
(31, 3, 9),
(49, 3, 10),
(45, 3, 11),
(43, 3, 15),
(48, 3, 19),
(44, 3, 20),
(38, 4, 1),
(39, 4, 3),
(19, 5, 4),
(20, 5, 5),
(50, 6, 1),
(51, 6, 16),
(53, 6, 17),
(40, 6, 18),
(52, 6, 19),
(1, 11, 1),
(21, 15, 3),
(33, 16, 4),
(34, 16, 5),
(36, 16, 8),
(35, 16, 16),
(37, 16, 26);

-- --------------------------------------------------------

--
-- Table structure for table `people`
--

CREATE TABLE `people` (
  `id` int NOT NULL,
  `person` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `people`
--

INSERT INTO `people` (`id`, `person`) VALUES
(1, 'Alireza'),
(15, 'Arsene'),
(20, 'Aurelien'),
(16, 'Aurore'),
(13, 'Danniel'),
(11, 'Enock'),
(6, 'Fatou'),
(3, 'Hava'),
(12, 'Hussam'),
(7, 'Mohammed'),
(27, 'Musta'),
(19, 'Nicholas'),
(14, 'Osman'),
(17, 'Raphael'),
(10, 'Remy'),
(8, 'Theo');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int NOT NULL,
  `task` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `task`) VALUES
(4, 'JAVASCRIPT'),
(6, 'NODE JS\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assigned_tasks3`
--
ALTER TABLE `assigned_tasks3`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `task_person` (`task`,`assigned_person`);

--
-- Indexes for table `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_person_name` (`person`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_task_name` (`task`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_tasks3`
--
ALTER TABLE `assigned_tasks3`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `people`
--
ALTER TABLE `people`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
