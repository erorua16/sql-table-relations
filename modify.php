<?php require "connection.php"; 
$id = $_REQUEST["id"];
ob_start();?>
<?php require "config.php";?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>UPDATE AN ITEM IN THE TABLE</h1>
<!-- FORM START -->
<form method="post">
    <?php 
    // START FOREACH
    foreach($result6 as $res6) {
    ?>
        <table>
        <th>Task</th>
        <th>Assigned to</th>
            <tr>
                <td>
                    <select name='task_is'>
                        <?php
                        //START LOOP//
                            foreach ($result2 as $res2) {
                            ?>
                            <option value= "<?= $res2{'id'} ?>"> 
                            <?= $res2['task']?>
                            </option>
                        <?php
                        //END LOOP//
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <select name='new_assignment'>
                        <?php
                        //START LOOP//
                            foreach ($result as $res) {
                            ?>
                            <option value="<?=$res{'id'}?>"> 
                            <?= $res['person']?>
                            </option>
                        <?php
                        //END LOOP//
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <input type='submit' name='modify' value='Submit'/>
                </td>
            </tr>
        </table>
    <!-- END FOREACH -->
    <?php
    }?>
    <!-- OLD TASK NAME -->
    <?php
        forEach($result6 as $res6){
            echo "old task was:" . $res6['task'] . "<br/>" . "old was assigned to:" .$res6['person'];
        }
    ?>
<!-- FORM END -->
</form>
<a href="./index.php"><button>GO BACK</button></a>
</body>
</html>
