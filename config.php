<!--/////////////////////////////////////////////////////QUERY TO GET ALL PEOPLE////////////////////////////////////////-->





<?php 
    $sql = "SELECT * FROM people";
    $result = $conn->query($sql);
?>




<!--////////////////////////////////////////////////////////////////QUERY TO ADD TO PEOPLE////////////////////////////////////////-->




<?php
$name_person = !empty ($_REQUEST["person_name"]) ? $_REQUEST["person_name"] : NULL;
if(!empty($name_person) === true){
    $sql = $conn->prepare("INSERT INTO people (person) VALUES (?)");
    $sql->bind_param('s', $name_person);
    $sql->execute();
    header('Location: ./index.php');
}
?>





<!--////////////////////////////////////////////////////////////////QUERY TO DELETE FROM PEOPLE////////////////////////////////////////-->




<?php
$delete_this_person  = !empty ($_REQUEST["delete_person"]) ? $_REQUEST["delete_person"] : NULL;
if(!empty($delete_this_person)){
    $sql11 = $conn->prepare("DELETE FROM people WHERE id= ?");
    $sql11->bind_param('i', $delete_this_person);
    $sql11->execute();
    header('Location: ./index.php');
}
?>






<!--/////////////////////////////////////////////////////QUERY FOR TASK TABLE/////////////////////////////////////////////////-->





<?php 
    $sql2 = "SELECT * FROM tasks";
    $result2 = $conn->query($sql2);
?>





<!--/////////////////////////////////////////////////////QUERY TO ADD TO TASK TABLE/////////////////////////////////////////////////-->






<?php
$name_task = !empty ($_REQUEST["task_name"]) ? $_REQUEST["task_name"] : NULL;
if(!empty($name_task) === true){
    $sql2 = $conn->prepare("INSERT INTO tasks (task) VALUES (?)");
    $sql2->bind_param('s', $name_task);
    $sql2->execute();
    header('Location: ./index.php');
}
?>




<!--////////////////////////////////////////////////////////////////////////QUERY TO DELETE TASK/////////////////////////////////////////////////////////////////////-->






<?php
$delete_this_task  = !empty ($_REQUEST["delete_task"]) ? $_REQUEST["delete_task"] : NULL;
if(!empty($delete_this_task)){
    $sql10 = $conn->prepare("DELETE FROM tasks WHERE id= ? ");
    $sql10->bind_param('i', $delete_this_task);
    $sql10->execute();
    header('Location: ./index.php');
}
?>






<!--////////////////////////////////////////////////QUERY TO GET ASSIGNED TASK AND PERSON DATA TO MODIFY////////////////////////////////////////////////////////////////-->





<?php 
$sql6 = "SELECT assigned_tasks3.* , tasks.task, tasks.id AS id2 , people.person, people.id AS id3 FROM assigned_tasks3 JOIN tasks ON tasks.id = assigned_tasks3.task JOIN people ON people.id = assigned_tasks3.assigned_person WHERE assigned_tasks3.id = $id";
$result6 = $conn->query($sql6);
?>





<!--/////////////////////////////////////////////////////////////////QUERY TO COMBINE TABLES AND LOOK AT THEM/////////////////////////////////////////////////////////////////////////-->





<?php 
$sql9 = "SELECT assigned_tasks3.* , tasks.task, people.person FROM assigned_tasks3 JOIN tasks ON tasks.id = assigned_tasks3.task JOIN people ON people.id = assigned_tasks3.assigned_person";
$result9 = $conn->query($sql9);
?>





<!--///////////////////////////////////////////////////////QUERY TO ADD TO ASSIGNED TABLE///////////////////////////////////////////////////////////////-->





<?php
$id_person = !empty($_REQUEST['person']) ? $_REQUEST['person'] : NULL;
$id_task = !empty($_REQUEST['idv_task']) ? $_REQUEST['idv_task'] : NULL;
if(!empty($id_person && $id_task) === TRUE){
    forEach($id_person as $idv_id){
        $sql3 = $conn->prepare ("INSERT INTO assigned_tasks3 (task, assigned_person) VALUES (?, ?)");
        $sql3->bind_param('ii',$id_task, $idv_id );
        $sql3->execute();
        header('Location: ./index.php');
    };
}
?>





<!--////////////////////////////////////////////////////////////////DELETE FROM ASSIGNED TABLE////////////////////////////////////////////////////////////////-->





<?php
$delete_task  = !empty ($_REQUEST["delete"]) ? $_REQUEST["delete"] : NULL;
if(!empty($delete_task)){
    $id = $_REQUEST['delete'];
    $sql4= $conn->prepare("DELETE FROM assigned_tasks3 WHERE id= ? ");
    $sql4->bind_param('i' , $id);
    $sql4->execute();
    header('Location: ./index.php');
}
?>  




<!--////////////////////////////////////////////////////////////////UPDATE TASK FROM ASSIGNED TABLE////////////////////////////////////////////////////////////////-->





<?php 
$assigned_task         =    !empty ((INT)$_REQUEST["task_is"]) ? (INT)$_REQUEST["task_is"]               : NULL;
$assign_to_someone     =    !empty ((INT)$_REQUEST["new_assignment"]) ? (INT)$_REQUEST["new_assignment"] : NULL ;
if(isset($_POST['modify'])){
    if($assigned_task  !== NULL && $assign_to_someone !== NULL){
        $sql7 = $conn->prepare("UPDATE assigned_tasks3 SET task = '$assigned_task', assigned_person = '$assign_to_someone' WHERE id= ?");
        $sql7->bind_param('i', $id);
        $sql7->execute();
        header('Location: ./index.php');
    };
}
?>